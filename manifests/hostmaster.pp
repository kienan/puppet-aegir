# Installs a hostmaster on a debian system using the available repos.
# This module should be called from a profile where the other major software
# eg, webserver and db server are decided on.
class aegir::hostmaster (
  # A hash with 'user', 'host', 'password'. Currently only mysql is supported by
  # aegir.
  Hash $database,
  String $frontend_url = $facts['fqdn'],
  # This is the major version only.
  Integer $version = 3,
  # The webserver, apache2 or nginx.
  $server = 'apache2',
  $manage_sources = true,
  # Commonly available releases: stable, unstable.
  String $release = 'stable',
  $makefile = '',
  $email = '',
  $working_copy = false,
  $drush_version = 'stable'
) {
  if $manage_sources {
    class { 'aegir::source':
      release => $release,
    }
    Class['aegir::source']
    -> Class['apt::update']
    -> Package['aegir']
  }
  $package_name = "aegir${version}"
  $working_copy_str = String($working_copy, '%s')
  $debconf_settings = [
    "${package_name}-hostmaster aegir/site string ${frontend_url}",
    "${package_name}-hostmaster aegir/db_password password ${database['password']}",
    "${package_name}-hostmaster aegir/db_host string ${database['host']}",
    "${package_name}-hostmaster aegir/db_user string ${database['user']}",
    "${package_name}-hostmaster aegir/webserver select ${server}",
    "${package_name}-hostmaster aegir/email string ${email}",
    "${package_name}-hostmaster aegir/makefile string ${makefile}",
    "${package_name}-hostmaster aegir/working-copy boolean ${working_copy_str}",
    "${package_name}-provision aegir/drush_version string ${drush_version}",
  ]
  file { '/var/lib/dpkg/aegir.response':
    ensure  => 'file',
    content => join($debconf_settings, "\n"),
  }
  if ($server == 'nginx') {
    # Install nginx and phpX-fpm before running the aegir install.
    case $facts['os']['lsb']['distcodename'] {
      'wheezy', 'jessie': { $php = ['php5-fpm'] }
      default: { $php = ['php7.0-fpm'] }
    }
    ensure_packages(
      concat(['nginx'], $php),
      {
        'ensure' => present,
        'before' => Package['aegir'],
      }
    )
  }
  # Pass in extra packages through install options for nginx support.
  # Even with aegir/webserver set in preconfig, apt-get tries to install
  # with phpX (not fpm).
  # Note: this may fail for some versions of Aegir:
  # @see https://www.drupal.org/project/provision/issues/2979947
  package { 'aegir':
    ensure       => present,
    name         => $package_name,
    responsefile => '/var/lib/dpkg/aegir.response',
    require      => File['/var/lib/dpkg/aegir.response'],
  }
}
