# aegir

## Module Description

This module is intended to install aegir simply.

## License

    Copyright (C) 2017 Kienan Stewart

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

## Requirements

Modules from forge.puppet.com:

* puppetlabs-stdlib (tested with 4.25.1)
* puppetlabs-apt (tested with 2.4.0)

Operating systems: Debian derivatives.

Puppet: 4.x

## Setup

Nothing special.

## Limitations

* Depends on the the aegir${version} package to correctly install dependencies

## Example Usage

### Install a hostmaster

```puppet
# Using puppetlabs/mysql (5.4.0) as example
class { '::mysql::server':
  package_name => 'default-mysql-server',
  service_name => 'mysqld',
  users        => {
    'aegirroot@localhost' => {
      'password_hash' => mysql_password('examplepassword'),
    },
  },
  grants       => {
    'aegirroot@localhost/*.*' => {
      'user'       => 'aegirroot@localhost',
      'table'      => '*.*',
      'privileges' => ['ALL'],
      'options'    => ['GRANT'],
    },
  },
}

# Assumes mysql or mariadb is installed
# and that there is a user aegir_root
# with all grants etc permissions on *.*
class { 'aegir::hostmaster':
  database => {
    host     => 'localhost',
    user     => 'aegirroot',
    password => 'examplepassword'
  }
}

Class['::mysql::server']
->Class['::aegir::hostmaster']
```

## Reference

### Classes

#### Public Classes

* aegir::hostmaster
* aegir::source

### Class: aegir::hostmaster

Debian package based hostmaster installation.

Parameters:

* database: A hash containing user, host, and password. Required.
* frontend_url: The url for the hostmaster drupal installation. Optional, defaults to the fully qualified domain name.
* version: The major aegir version to install. Optional, default: 3
* server: Which webserver to use. Optional, default: apache. Supports: apache2, nginx
* manage_sources: Add source for Aegir packages. Default: true
* release: Which release to use from the package source. Optional, default: stable.
* makefile: If a custom makefile should be used, specify it here. Optional, default: ''
* email: The email to set for the admin user in the hostmaster installation. Optional, default: '' (aegir will pick the user@fqdn).
* working_copy: Keep the hostmaster platform git directories? Optional, default: false.
* drush_version: Which version of drush to install with provision. Optional, default '' (stable).

### Class: aegir::source

Adds the debian apt source

Parameters:

* release: Which release of aegir to use. Default: stable
