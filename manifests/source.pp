class aegir::source (
  String $release = 'stable'
) {
  apt::source { 'aegir':
    location => 'http://debian.aegirproject.org',
    release  => $release,
    key      => {
      id      => '12782E2257B468806EF36D165ADF93A03376CCF9',
      content => file('aegir/key.asc'),
    },
    repos    => 'main'
  }
}
