require 'spec_helper'
describe 'aegir::hostmaster' do
  let(:title) { 'aegir' }
  let(:params) {
    { 'database' =>
      { 'host' => 'localhost',
       'user' => 'aegir_root',
       'password' => 'password' },
      'manage_sources' => false
    }
  }
  # Need to fill out a fair amounts of facts for the apt module.
  let(:facts) {
    {
      :operatingsystem => 'Debian',
      :osfamily => 'Debian',
      :os => {
        'name' => 'Debian',
        'lsb' => {
          'distcodename' => 'stretch'
        },
        'release' => {
          'full' => '',
          'major' => '9'
        }
      }
    }
  }

  it do
    is_expected.to contain_file('/var/lib/dpkg/aegir.response')
    is_expected.to contain_package('aegir').with(
      'ensure' => 'present',
      'name'   => 'aegir3'
    )
  end

  context 'with manage_sources => true' do
    let(:params) {
      { 'database' =>
        {'host' => 'localhost',
         'user' => 'aegir_root',
         'password' => 'password' },
        'manage_sources' => true
      }
    }
    it { is_expected.to contain_class('aegir::source') }
  end

  context 'with server => nginx' do
    let(:params) {
      { 'database' =>
        { 'host' => 'localhost',
          'user' => 'aegir_root',
          'password' => 'password' },
        'manage_sources' => false,
        'server' => 'nginx'
      }
    }
    it do
      is_expected.to contain_package('nginx').with_ensure('present')
      is_expected.to contain_package('php7.0-fpm').with_ensure('present')
    end
  end
end
